const { MongoClient } = require('mongodb')
const { Observable } = require('rxjs')
const bson = require('bson')

function regex(pattern) {
	pattern = pattern || '*';
	if(pattern.indexOf('*') < 0) return pattern;
	pattern = pattern.replace(/[*]/g, '(.*?)');
	return new RegExp('^' + pattern + '$', 'i');
}

function parseMongoConnectionString(mongo_connection) {
	const [, , _hosts, , name, params] = mongo_connection.match(/^(mongodb:\/\/)?([^\/]+)+(\/([\w]+))?(\?.*)?$/) || []
	const hosts = _hosts.split(',')
	if(!hosts) throw new Error('Bad mongo connection string')
	return { hosts, name, params }
}

function formatMongoConnection(mongo_connection) {
	const {hosts, params} = parseMongoConnectionString(mongo_connection)
	return `mongodb://${hosts}/local${params || ''}`
}

function isBsonTimestamp(ts) {
	return ts instanceof bson.Timestamp ||
		(
			ts._bsontype === 'Timestamp' &&
			ts.hasOwnProperty('low_') &&
			ts.hasOwnProperty('high_')
		)
}

function formatTimestamp(ts) {
	if(!ts) return null
	switch(true) {
		case isBsonTimestamp(ts):
			return ts
		case typeof ts === 'string':
			return bson.Timestamp.fromString(ts)
		default:
			console.warn('Invalid timestamp', ts)
			return null
	}
}

function oplog(mongo_connection, ns, from_ts, { all_ops, full_logs, await_time_ms } = {}) {
	
	from_ts = formatTimestamp(from_ts)
	
	return Observable.create(observer => {
		let _db, _open
		mongo_connection = formatMongoConnection(mongo_connection)
		MongoClient.connect(mongo_connection)
		.then(db => {
			_db = db
			_open = true
			const query = Object.assign({ ns: regex(ns) },
				from_ts    && { ts: { $gt: from_ts } },
				!all_ops   && { op: { $in: ['i','u','d'] } }
			)
			
			let cursor = db
				.collection('oplog.rs')
				.find(query)
				.maxAwaitTimeMS(await_time_ms || 1000)
		  		.addCursorFlag('awaitData', true)
				.addCursorFlag('tailable', true)
			
			cursor = !full_logs ?
				cursor.project({ 'ts' : 1, 't' : 1, 'h' : 1, 'v' : 1, 'op' : 1, 'ns' : 1, 'o._id': 1, 'o2._id' : 1 }) :
				cursor

			function getNext() {
				cursor.next()
					.then(doc => observer.next(doc))
					.then(() => _open && getNext())
					.catch(err => observer.error(err))
			}

			getNext()
		})
		.catch(err => observer.error(err))

		return () => {
			if(_open) {
				_open = false
				_db.close()
				console.log('Oplog disposed')
			}
		}
	})
}

module.exports = oplog